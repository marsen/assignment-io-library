section .text

;IN 	rdi : exit code
;OUT	-
;		exit with given code
exit: 
		mov rax, 60 ;exit code is already in rdi
		syscall

;IN	 	rdi : message start (message ends with "0")
;OUT	rax : message length
string_length:
		xor rax, rax					;clear rax to count chars in it
.loop:	inc rax							;rax++
		cmp byte [rdi + rax - 1], 0		;compare current (begining + count - 1) byte with zero
		jne .loop						;if equals and current byte is zero it's end of line
		dec rax							;rax-- because we don't count the last symbol == 0
		ret

;IN		rdi : message start (message ends with "0")
;OUT	prints message to stdout
print_string:
		push rdi			;put rdi on stack because str_len can change it
		;mov rsi, rdi 		;put message to syscall parameter
		call string_length	;we can call it because message already in rdi
		pop rsi				;put rdi (message start) to rsi
		mov rdx, rax		;put length to syscall parameter
		mov rax, 1			;syscall will be writing
		mov rdi, 1			;stdout descriptor
		syscall
		ret


;IN		nothing
;OUT	prints new line symbol
print_newline:
		mov rdi, 0xA

;IN		rdi : char code number
;OUT	prints char to stdout
print_char:
		push rdi		;save char in memory, because syscall need the pointer not value
		mov rsi, rsp 	;put char pointer to rsi syscall parameter
		mov rax, 1
		mov rdi, 1
		mov rdx, 1		;message len is 1 because we print 1 char
		syscall
		pop rdi			;clear stack to be able come back
		ret

;IN		rdi : int number
;OUT	print it
print_int:
		cmp rdi, 0				;compare argument with 0
		jge .print_positive		;if it >= 0 -- it's positive, we can print it
		neg rdi					;if argument is negatine number we have to print it with '-'
		push rdi				;so, we make it positive and save to stack
		mov rdi, '-'			;to be able to use rdi-argument register to call
		call print_char			;function that will print '-' for us
		pop rdi					;then we get our number back and have to print it after '-'
.print_positive:				;label to print positive numbers

;IN		number
;OUT	print number in 10, in ASCII
print_uint:
		mov rax, rdi		;the whole part of devision
		mov rcx, 10			;the system base
		push 0				;put 0 terminator to stack,
		mov rsi, rsp		;then move stack top to size of
		sub rsp, 21			;the number buffer (20 numbers + 0 terminator)
.print_uint_loop:
		xor rdx, rdx		;the remainder of devision has o be zero at start
		div rcx				;devide the whole part by 10
		add dl, 0x30		;in dl (the little byte of rdx) + 0x30 to make it ASCII symbol
		dec rsi				;decrement the size of rest of max string len
		mov [rsi], dl		;put last symbol to stack
		test rax, rax		;if rax is equal to 0 we finised parse number
		jnz .print_uint_loop
		mov rdi, rsi		;put start position of buffer
		call print_string	;print number
		add rsp, 21			;return stack pointer back
		pop rdi				;because we put 0 at first
		ret


;IN		rdi : st message start; rsi : 2nd message start (both messages ends with "0")
;OUT	1 if equals; 0 - else
string_equals:
		xor rax, rax						;in rax would be conter of symbols
.string_equals_loop:
		mov r8b, byte [rdi + rax]			;copy byte from 1st message
		mov r9b, byte [rsi + rax]			;copy byte from 2nd message
		inc rax
		cmp r8b, r9b						;cmp first message symbol and second ones
		jne .fail							;fail if not equals
		test r8b, r8b				;if end of line return result, else keep contrasting messages
		jne .string_equals_loop
		mov rax, 1							;messages are equal
		ret
.fail
		mov rax, 0
		ret

;IN		-
;OUT		rax : char from stdin, or 0 it it's the end of stream
read_char:
		push 0				;save 0 to stack to put there result even if we faild to read
		mov rax, 0			;the number of syscall to read
		mov rdi, 0			;stdin descriptor number
		mov rsi, rsp		;we use stack as buffer
		mov rdx, 1			;we need to read 1 char
		syscall				;do syscall
		pop rax				;get result from stack
		ret

;IN		rdi : buffer pointer, rsi : buffer size
;OUT	put word from stdin to buffer (skip 0x20, 0x9, 0xA)
;		last symbol has to be 0
;		return buffer pointer in rax and word len in rdx
;		or 0 in rax if word is bigger than buffer
read_word:
		test rsi, rsi						;if buffer size is 0 we can't continue
		jne .continue						;and we have to return
		xor rax, rax
		ret
.continue:
		push r12							;save callee-saved register to use it
		push r13
		push r14
		mov r14, 0							;counter of curretn amount of symbols in word
		mov r12, rdi						;buffer pointer
		mov r13, rsi						;buffer size
.whitespace_char:
		call read_char						;read char
		cmp rax, 0x20						;check if whitespace
		je .whitespace_char		
		cmp rax, 0x9						;check if tab
		je .whitespace_char
		cmp rax, 0xA						;check if \n
		je .whitespace_char
		test rax, rax						;chack if it's not 0
		je .fail_finish
.word:
		mov byte[r12 + r14], al				;write symbol (less byte of rax) to the buffer
		inc r14								;inc pointer, because we jest put new element
		cmp r14, r13						;Check if we can read one more symbol
		je .fail_finish						;if rdi == rcx buffer is filled, so we can't read more
		call read_char						;if buffer isn't filled we can read one more symbol
		cmp rax, 0x20						;check if whitespace
		je .finish
		cmp rax, 0x9						;check if tab
		je .finish
		cmp rax, 0xA						;check if /n
		je .finish
		test rax, rax						;check if it's the end of input
		jne .word
.finish:
		mov byte[r12 + r14], 0				;terminator symbol
		mov rax, r12						;put buffer pointer in rax
		mov rdx, r14						;put word size
		jmp .return
.fail_finish:
		xor rax, rax
		xor rdx, rdx
.return:
		pop r14								;restore callee-saved registers
		pop r13
		pop r12
		ret


;IN		rdi : line pointer
;OUT	rax : unsigned number from line, rdx : it length
;		if fail -- return 0 in rdx
parse_uint:
		xor r9, r9					;counter of symbols, we use r9 cose mul use rdx
		xor r8, r8					;current symbol
		xor rax, rax				;final number
		mov rcx, 10					;conting base
;read first symbol
		mov r8b, byte[rdi + r9]		;read first symbol
		sub r8b, '0'				;convert code to number
		jl .parsing_error			;if negative it's NaN
		cmp r8b, 9					;if it's greater then '9' it's NaN too
		ja .parsing_error
		add rax, r8					;add last number
		inc r9						;inc counter because our number become bigger for 1
		test rax, rax				;if symbol = 0 it's the end of the number
		je .okey_finish
.read_next_symbol:
		mov r8b, byte[rdi + r9]
		sub r8b, '0'
		jl .okey_finish
		cmp r8b, 9
		ja .okey_finish
		mul rcx
		add rax, r8
		inc r9
		jmp .read_next_symbol
.parsing_error:
		xor rdx, rdx
		ret
.okey_finish:
		mov rdx, r9
		ret


;IN		rdi : line pointer
;OUT	rax : number we read from line, rdx : it len, sign is counted
;		rdx = 0 if we failed to read
;		no whitespaces between sign and number are allowed
parse_int:
		push r12					;save callee-saved register
		mov r12, 1					;sign is positive by default
		xor r8, r8					;first char
		mov r8b, byte[rdi]			;put first char to less byte of r8
		cmp r8b, 0x2d				;if it's "-" number is negative
		jne .positive				;jump to reading positive number
;negative number parsing
		mov r12, -1					;set sign negative
		inc rdi						;if first symbol is "-" we start from 2 symbol
.positive:
		call parse_uint				;read unsigned number
		cmp rdx, 0					;if rdx -  we fail to read number
		je .ret						;in rax - number in rdx it len
		cmp r12, 1					;if num was positive
		je .ret						;if it's negative we have to: *(-1) and inc len
		inc rdx						;inc it len because of "-"
		push rdx					;save rdx because it's used by mul
		mul r12						;mul value by -1
		pop rdx						;get len back
.ret:
		pop r12:						; restore callee-saved register
		ret					;rdx is already 0

;IN		rdi : message start, rsi : buffer to put message begining, rdx : buffer length
;OUT 	length of message if sucsess, or 0 if fail
string_copy:
		xor rax, rax					;in rax we collect message
.string_copy_loop:						;loop by chars
		cmp rax, rdx					;check if message didn't end, rax and rdx can't be equal,
		je .out_of_bounds				;because it will mean the end of the buffer
		mov r8, [rdi]					;put value from message buffer
		mov [rsi], r8
		inc rax							;len in buffer is bigger for one now
		inc rsi							;incriment pointer in buffer
		inc rdi							;incriment pointer in message
		test r8, 0xff						;if it wasn't the last element -- continue
		jne .string_copy_loop			;else -- return, rax already contains len
		ret
.out_of_bounds:
		mov rax, 0
		ret
